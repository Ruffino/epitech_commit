export declare class CreateUserDto {
    readonly username: string;
    password: string;
    email: string;
}
