import { HttpStatus } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from '../auth/interfaces/user.interface';
export declare class UsersController {
    usersService: any;
    getProfile(req: any): Promise<User>;
    deleteItem(id: any): Promise<{
        status: HttpStatus;
    }>;
    updateItem(id: any, createUserDto: CreateUserDto): Promise<{
        status: HttpStatus;
    }>;
}
