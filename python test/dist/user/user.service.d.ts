import { User } from '../auth/interfaces/user.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { Model } from 'mongoose';
export declare class UsersService {
    private readonly userModel;
    private hashLength;
    constructor(userModel: Model<User>);
    delete(id: number): Promise<any>;
    updateItem(id: string, createUserDto: CreateUserDto): Promise<User>;
    getHash(password: string): Promise<string>;
}
