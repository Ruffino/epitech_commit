import { Model } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { User } from './interfaces/user.interface';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
export declare class AuthService {
    private userModel;
    private jwtService;
    constructor(userModel: Model<User>, jwtService: JwtService);
    signUp(authCredentialsDto: AuthCredentialsDto): Promise<void>;
    signIn(user: User): Promise<{
        accessToken: string;
    }>;
    validateUser(email: string, pass: string): Promise<User>;
}
