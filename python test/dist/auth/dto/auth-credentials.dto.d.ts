export declare class AuthCredentialsDto {
    username: string;
    email: string;
    password: string;
}
