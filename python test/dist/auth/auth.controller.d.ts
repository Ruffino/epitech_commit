import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    register(authCredentialDto: AuthCredentialsDto): Promise<void>;
    signIn(req: any): Promise<{
        accessToken: string;
    }>;
}
