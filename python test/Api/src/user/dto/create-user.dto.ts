import { IsString } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class CreateUserDto {
    @ApiPropertyOptional()
    @IsString() readonly username: string;

    @ApiPropertyOptional()
    @IsString() password: string;

    @ApiPropertyOptional()
    @IsString() email: string;

}
