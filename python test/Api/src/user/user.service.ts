import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { User } from '../auth/interfaces/user.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
    private hashLength = 16;
    constructor(@Inject('UserModelToken') private readonly userModel: Model<User>) { }

    async delete(id: number): Promise<any> {
        try {
            await this.userModel.deleteOne({
                _id: id,
            });
        } catch (error) {
            if (error.code === 400) {
                throw new BadRequestException('User unfound');
            }
            throw error;
        }
    }

    async updateItem(id: string, createUserDto: CreateUserDto): Promise<User> {
        createUserDto.password = await this.getHash(createUserDto.password);
        return await this.userModel.updateOne({_id: id }, createUserDto);
    }

    async getHash(password: string): Promise<string> {
        return bcrypt.hash(password, this.hashLength);
    }


}
