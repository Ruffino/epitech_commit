import {
  Controller,
  Get,
  Request,
  Delete,
  Param,
  HttpStatus,
  Patch,
  Body,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { ApiTags } from '@nestjs/swagger';
import { User } from '../auth/interfaces/user.interface';

@Controller('user')
@ApiTags('User')
export class UsersController {
  usersService: any;
  @Get()
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  getProfile(@Request() req): Promise<User> {
    return req.user;
  }

  @Delete(':id')
  async deleteItem(@Param('id') id) {
    this.usersService.delete(id);
    return { status: HttpStatus.OK };
  }

  @Patch(':id')
  async updateItem(@Param('id') id, @Body() createUserDto: CreateUserDto) {
    this.usersService.updateItem(id, createUserDto);
    return { status: HttpStatus.OK };
  }
}
