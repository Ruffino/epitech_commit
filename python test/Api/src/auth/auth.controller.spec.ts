import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './Auth.service';
import { HttpStatus } from '@nestjs/common';
import { User } from './interfaces/user.interface';

describe('Auth Controller', () => {
  let authController: AuthController;
  let authService: AuthService;
  
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        controllers: [AuthController],
        providers: [AuthService],
      }).compile();

      authService = moduleRef.get<AuthService>(AuthService);
      authController = moduleRef.get<AuthController>(AuthController);
  });

  describe('findAll', () => {
    it('should rdelete a user', async () => {
      const result =  <any> 
      jest.spyOn(authService, 'signUp').mockImplementation(() => result);
      let user: User 
      expect(await authController.register(user)).toBe(1);
    });
  });
});