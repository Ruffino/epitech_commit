import spacy
from spacy_langdetect import LanguageDetector
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from spacy import displacy

nlp = spacy.load('fr')
nlp.add_pipe(LanguageDetector(), name='language_detector', last=True)
stemmer = SnowballStemmer(language='french')

file = open("text.txt","r")
content = file.read()
doc = nlp(content)


for sent in doc.sents:
   print(sent, sent._.language)
   if(sent._.language.get('language') != 'fr'):
       print('pas fr')
   else:
        print('fr')




root = [token for token in doc if token.head == token][0]
subject = list(root.lefts)[6]
for descendant in subject.subtree:
    assert subject is descendant or subject.is_ancestor(descendant)
    print(descendant.text, descendant.dep_, descendant.n_lefts,
            descendant.n_rights,
            [ancestor.text for ancestor in descendant.ancestors])


def return_token_sent(sentence):
    doc = nlp(sentence)
    return [X.text for X in doc.sents]

def return_NER(sentence):
    doc = nlp(sentence)
    return [(X.text, X.label_) for X in doc.ents]

def return_POS(sentence):
    doc = nlp(sentence)
    return [(X, X.pos_) for X in doc]


'''
doc = nlp(u'Mark and John are sincere employees at Google.')
noun_adj_pairs = []
for i,token in enumerate(doc):
    if token.pos_ not in ('NOUN','PROPN'):
        continue
    for j in range(i+1,len(doc)):
        if doc[j].pos_ == 'ADJ':
            noun_adj_pairs.append((token,doc[j]))
            break
noun_adj_pairs


for token in nlp(text):
    if token.dep_ == 'nsubj': # Or other forms of subjects / objects
        print(token.lemma_+"'s are:")
        for a in token.ancestors:
            if a.text == 'are': # Or however you determine your selection
                for atok in a.children:
                    if atok.dep_ == 'acomp': # Note, you should look for more than just acomp
                        print(atok.text)
'''