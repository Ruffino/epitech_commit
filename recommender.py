import pandas as pd
import os
import numpy as np
from matplotlib import pyplot as plt
from sklearn.datasets.samples_generator import make_blobs
from sklearn.cluster import KMeans
from sklearn import preprocessing
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin_min


dataset = pd.read_csv("KaDo.csv", sep = ',')


univers_value = {'CAP_AP SHAMP' : 0, 'CAP_SHAMP SPECIFIQUE':100, 'CAP_SHAMP TRAITANT':101, 'CAP_SHAMP TSCHEVEUX':102, 'CAP_TENUE DE LA COIFFURE':200, 'CAP_TRAITEMENT':300, 'CORPS_EAUX CORPS AUTRES':1000, 'CORPS_EAUX CORPS MONOI':1001, 'CORPS_EXFOLIANT CORPS':1100, 'CORPS_HYDRA NOURRISANT':1200, 'CORPS_LAIT HUILE AUTRES':1300, 'CORPS_LAIT HUILE MONOI':1301, 'CORPS_LAIT HUILE PARFUMS':1302, 'CORPS_LAIT HUILE PLAISIRNATURE':1303, 'CORPS_PRODUITS MINCEUR':1400, 'CORPS_SOIN DES MAINS':1500, 'CORPS_SOIN PIEDS ET JAMBES':1501, 'CORPS_SOINS HAMMAM':1502, 'DIETETIQUE':2000, 'HYG_BAIN SEL HUILE PERLE':3000, 'HYG_DEO AUTRES':3100, 'HYG_DEO CULTURE BIO':3101, 'HYG_DEO HOMMES':3102, 'HYG_DEO JARDINMONDE':3103, 'HYG_DEO PARFUMS':3104, 'HYG_DOUCHE AUTRES':3200, 'HYG_DOUCHE CULTUREBIO':3201, 'HYG_DOUCHE EDT ETE':3202, 'HYG_DOUCHE EDT NOEL':3203, 'HYG_DOUCHE FRAICHEUR VEG':3204, 'HYG_DOUCHE HAMMAM':3205, 'HYG_DOUCHE HOMMES':3206, 'HYG_DOUCHE JARDINMONDE':3207, 'HYG_DOUCHE MONOI':3208, 'HYG_DOUCHE PARFUMS':3209, 'HYG_D0UCHE PLAISIRNATURE':3210, 'HYG_RASAGE':3300, 'HYG_SAVONS':3400, 'MAQ_Autres':4000, 'MAQ_LEV AUTRES':4100, 'MAQ_LEV BRILLANT':4201, 'MAQ_LEV RAL Autres':4202, 'MAQ_LEV RAL Brillance':4203, 'MAQ_LEV RAL GrdRouge':4204, 'MAQ_LEV RAL Hydr':4205, 'MAQ_LEV RAL Lum4':4206, 'MAQ_ONG AUTRES':4300, 'MAQ_ONG Vernis CN':4301, 'MAQ_ONG Vernis LUM':4302, 'MAQ_TEINT Fard a joues':4400, 'MAQ_TEINT Font teint':4401, 'MAQ_TEINT Perfect Correcteur':4402, 'MAQ_TEINT Poudres':4403, 'MAQ_YEUX Autres':4500, 'MAQ_YEUX Crayons':4501, 'MAQ_YEUX Eyeliner':4502, 'MAQ_YEUX Fard':4503, 'MAQ_YEUX Mascara Basic':4504, 'MAQ_YEUX Mascara Elixir':4505, 'MAQ_YEUX Mascara SexyPulp':4506, 'MAQ_YEUX Mascara Specifique':4507, 'MAQ_YEUX Mascara Vertige':4508, 'MAQ_YEUX Sourcils':4509, 'MAQ_YEUX Stylos':4510, 'MULTI FAMILLES':5000, 'PARF_APRES RASAGE':6000, 'PARF_EAUX AUTRES':6100, 'PARF_EAUX DE COLOGNE':6101, 'PARF_EAUX EDIT NOEL ETE':6102, 'PARF_EAUX HOMMES':6103, 'PARF_EAUX MONOI':6104, 'PARF_EAUX PARFUMS':6105, 'PARF_EAUX PLAISIR NATURE':6106, 'PARF_PARF ET EAU DE PARF':6200, 'SOINS CORPS ANTI-AGE':7000, 'SOL_APRES SOLEIL':8000, 'SOL_AUTOBRONZANTS':8100, 'SOL_PROTECTEURS SOLAIRES':8200, 'VIS_CJOUR AAAR':9000, 'VIS_CJOUR Autres':9001, 'VIS_CJOUR BIO':9002, 'VIS_CJOUR Jeunes Specifique':9003, 'VIS_CJOUR PUR':9004, 'VIS_CNUIT AAAR':9100, 'VIS_CNUIT Autres':9101, 'VIS_CNUIT BIO':9102, 'VIS_CNUIT Jeunes Specifique':9102, 'VIS_DEMAQ AAAR':9200, 'VIS_DEMAQ Autres':9201, 'VIS_DEMAQ BIO':9202, 'VIS_DEMAQ BLEUET':9203, 'VIS_DEMAQ Jeunes Specifique':9204, 'VIS_DEMAQ PUR':9205, 'VIS_LOTIONS AAAR':9300, 'VIS_LOTIONS Autres':9301, 'VIS_LOTIONS BIO':9302, 'VIS_LOTIONS Jeunes Specifique':9303, 'VIS_MASQUE AAAR':9400, 'VIS_MASQUE Jeunes Specifique':9401, 'VIS_MASQUE PUR':9402, 'VIS_SOIN HOMMES':9500, 'VIS_SOIN LEVRES':9501, 'VIS_TRAIT AAAR':9502, 'VIS_TRAIT BIO':9503, 'VIS_TRAIT Jeunes Specifique':9504}
df = dataset["UNIVERS"].replace(univers_value, inplace=True)



le = preprocessing.LabelEncoder()
y = le.fit_transform(dataset['LIBELLE'])
x = dataset['UNIVERS'].replace(univers_value, inplace=True)

print(df)


data_set = np.dstack((x,y))
data_set = data_set[0]


"""
distortions = []
K = range(1,10)
for k in K:
    kmeanModel = KMeans(n_clusters=k)
    kmeanModel.fit(data_set)
    distortions.append(kmeanModel.inertia_)

plt.figure(figsize=(16,8))
plt.plot(K, distortions, 'bx-')
plt.xlabel('k')
plt.ylabel('Distortion')
plt.title('The Elbow Method showing the optimal k')
plt.show()
"""

num_clusters = 9
model = KMeans(num_clusters).fit(data_set)

	
print(model.cluster_centers_)

def closest_node(node, nodes):
    nodes = np.asarray(nodes)
    dist_2 = np.sum((nodes - node)**2, axis=1)
    return np.argmin(dist_2)
#maille = dataset['MAILLE'].unique()
#print("\n",sorted(maille))


#univers = dataset['UNIVERS'].unique()
#print("\n",sorted(univers))

#famille = dataset['FAMILLE'].unique()
#print("\n",sorted(famille))


#print(dataset[dataset['UNIVERS']=='CAP_AP SHAMP']['LIBELLE'].unique())

#print(dataset.head())
#dataset_2 = dataset.merge('PRIX_NET', 'LIBELLE', on='TICKET_ID')
#print(dataset_2.head())




for i in range(len(data)):
    print("data: %s" % str(data[i, :]))
    for x in range(kmeans.n_clusters):
        min_dist = min(dists[i, kmeans.labels_ == x])
        max_dist = max(dists[i, kmeans.labels_ == x])
        print("cluster: %d\n\tcloses: %s: %g\n\tfarthest: %s: %g" 
              % (x, 
                 str(X[dists[i, :] == min_dist, :]),
                 min_dist,
                 str(X[dists[i, :] == max_dist, :]),
                 max_dist))