import spacy


nlp = spacy.load("./")

test_text = "J'arrive a Marseille et je pars de Paris"
doc = nlp(test_text)
for ent in doc.ents:
    print(ent.label_, ent.text)